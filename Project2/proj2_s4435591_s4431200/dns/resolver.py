#!/usr/bin/env python2

""" DNS Resolver

This module contains a class for resolving hostnames. You will have to implement
things in this module. This resolver will be both used by the DNS client and the
DNS server, but with a different list of servers.
"""

import socket

from dns.cache import RecordCache
from dns.classes import Class
from dns.message import Message, Header, Question
from dns.rcodes import RCode
from dns.types import Type

ROOT_SERVERS = [
    '198.41.0.4',
    '192.228.79.201',
    '192.33.4.12',
    '199.7.91.13',
    '192.203.230.10',
    '192.5.5.241',
    '192.112.36.4',
    '198.97.190.53',
    '192.36.148.17',
    '192.58.128.30',
    '193.0.14.129',
    '199.7.83.42',
    '202.12.27.33'
]

class Resolver(object):
    """ DNS resolver """
    
    def __init__(self, hints, timeout, caching, ttl):
        """ Initialize the resolver
        
        Args:
            caching (bool): caching is enabled if True
            ttl (int): ttl of cache entries (if > 0)
        """
        self.hints = ROOT_SERVERS + hints
        self.timeout = timeout
        self.caching = caching
        self.ttl = ttl
        if self.caching:
            self.cache = RecordCache(ttl)
            self.cache.read_cache_file()

    def __del__(self):
        """ Destructor of the resolver """
        if self.caching:
            self.cache.write_cache_file()

    def gethostbyname(self, hostname):
        """ Translate a host name to IPv4 address.

        Currently this method contains an example. You will have to replace
        this example with the algorithm described in section
        5.3.3 in RFC 1034.

        Args:
            hostname (str): the hostname to resolve

        Returns:
            (str, [str], [str]): (hostname, aliaslist, ipaddrlist)
        """
        # Remove trailing dots since we don't need them
        hostname = hostname.rstrip(".")
        aliases = []
        addresses = []

        if self.caching:
            # Handle caching for invalid FQDNs
            for invalid in self.cache.lookup(hostname, Type.SOA, Class.IN):
                return invalid.name, [], [invalid.rdata.data]

            # Get aliases from cache
            for alias in self.cache.lookup(hostname, Type.CNAME, Class.IN):
                aliases.append(alias.rdata.data)

            # Get addresses from cache and return if we found results
            for address in self.cache.lookup(hostname, Type.A, Class.IN):
                addresses.append(address.rdata.data)
            if addresses != []:
                return hostname, aliases, addresses

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(self.timeout)

        while self.hints != []:
            hint = self.hints.pop(0)

            # Arbitrarily chosen
            identifier = 9001

            # Create and send query
            question = Question(hostname, Type.A, Class.IN)

            header = Header(identifier, 0, 1, 0, 0, 0)
            header.qr = 0
            header.opcode = 0
            header.rd = 0
            query = Message(header, [question])

            sock.sendto(query.to_bytes(), (hint, 53))
            try:
                data = sock.recv(512)
            except:
                continue
            response = Message.from_bytes(data)

            # If the response identifier does not match the identifier
            # we sent we immediately abort as specified by RFC1034.
            if response.header.ident != identifier:
                continue

            # Cache invalid FQDNs
            if self.caching and response.header.rcode == RCode.NXDomain:
                response.authorities[0].name = hostname
                # The given framework has a bug where it cannot correctly go from
                # bytes to a string if the response is a name error. As a workaround
                # we set the data to "WorkaroundBug"
                response.authorities[0].rdata.data = "WorkaroundBug"
                self.cache.add_record(response.authorities[0])
                return hostname, [], []

            answer = False

            for answer in response.answers:
                # We always cache responses that are of type A and CNAME
                if self.caching and (answer.type_ == Type.A or answer.type_ == Type.CNAME):
                    self.cache.add_record(answer)

                if answer.type_ == Type.A and (answer.name == hostname or answer.name in aliases):
                    addresses.append(answer.rdata.data)
                    answer = True
                # CNAME in the Answer section; consider it an alias for now
                elif (answer.type_ == Type.CNAME and (hostname == answer.name or answer.name in aliases)
                        and answer.rdata.data not in aliases):
                    aliases.append(answer.rdata.data)

            # The answer section only contained CNAMEs. As a result
            # we will start searching for one of those instead.
            if answer == False and aliases != []:
                for alias in aliases:
                    _, al, ad = self.gethostbyname(alias)
                    # We want to return the original hostname query
                    if ad != []:
                        return hostname, al + aliases, ad

            for additional in response.additionals:
                # We always cache responses that are of type A and CNAME
                if self.caching and (additional.type_ == Type.A or additional.type_ == Type.CNAME):
                    self.cache.add_record(additional)

                # CNAME in the Additional section; it is an alias
                if (additional.type_ == Type.CNAME and (hostname == answer.name or answer.name in aliases)
                        and additional.rdata.data not in aliases):
                    aliases.append(additional.rdata.data)

            if addresses != []:
                return hostname, aliases, addresses

            authorities = []
            for authority in response.authorities:
                if authority.type_ == Type.NS:
                    for additional in response.additionals:
                        if additional.type_ == Type.A and additional.name == authority.rdata.data:
                            self.hints.insert(0, additional.rdata.data)

        return hostname, [], []
