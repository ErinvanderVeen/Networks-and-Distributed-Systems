#!/usr/bin/env python2

"""A cache for resource records

This module contains a class which implements a cache for DNS resource records,
you still have to do most of the implementation. The module also provides a
class and a function for converting ResourceRecords from and to JSON strings.
It is highly recommended to use these.
"""

import json
import time

from dns.resource import ResourceRecord, RecordData
from dns.types import Type
from dns.classes import Class

class ResourceEncoder(json.JSONEncoder):
    """ Conver ResourceRecord to JSON
    
    Usage:
        string = json.dumps(records, cls=ResourceEncoder, indent=4)
    """
    def default(self, obj):
        if isinstance(obj, ResourceRecord):
            return {
                "name": obj.name,
                "type": Type.to_string(obj.type_),
                "class": Class.to_string(obj.class_),
                "ttl": obj.ttl,
                "rdata": obj.rdata.data,
                "timestamp": obj.timestamp
            }
        return json.JSONEncoder.default(self, obj)

def resource_from_json(dct):
    """ Convert JSON object to ResourceRecord
    
    Usage:
        records = json.loads(string, object_hook=resource_from_json)
    """
    name = dct["name"]
    type_ = Type.from_string(dct["type"])
    class_ = Class.from_string(dct["class"])
    ttl = dct["ttl"]
    rdata = RecordData.create(type_, dct["rdata"])
    timestamp = dct["timestamp"]
    return ResourceRecord(name, type_, class_, ttl, rdata, timestamp)

class RecordCache(object):
    """ Cache for ResourceRecords """

    def __init__(self, ttl):
        """ Initialize the RecordCache
        
        Args:
            ttl (int): TTL of cached entries (if > 0)
        """
        self.records = []
        self.ttl = ttl

    def lookup(self, dname, type_, class_):
        """ Lookup resource records in cache

        Lookup for the resource records for a domain name with a specific type
        and class.
        
        Args:
            dname (str): domain name
            type_ (Type): type
            class_ (Class): class
        """
        records = []
        for record in self.records:
            # First check timestamp so that we can remove cached data that has expired.
            # This helps to keep the cache small since records we do not lookup will also
            # get removed.
            if record.timestamp + record.ttl > (int)(time.time()):
                if record.name == dname and record.type_ == type_ and record.class_ == class_:
                    records.append(record)
            else:
                self.records.remove(record)

        return records
    
    def add_record(self, record):
        """ Add a new Record to the cache
        
        Args:
            record (ResourceRecord): the record added to the cache
        """
        # Only add something to the cache if there is no entry of it yet
        if self.lookup(record.name, record.type_, record.class_) == []:
            if self.ttl > 0:
                record.ttl = self.ttl
            self.records.append(record)

    def read_cache_file(self):
        """ Read the cache file from disk """
        try:
            file = open('cache', 'r')
            self.records = json.loads(file.read(), object_hook=resource_from_json)
        except (IOError, ValueError):
            # Unable to read Cache file so consider it as an empty JSON file.
            # When writing it will automatically get created. An alternative option
            # would be to stop the program.
            self.records = json.loads("[]", object_hook=resource_from_json)

    def write_cache_file(self):
        """ Write the cache file to disk """
        try:
            open('cache', 'w').write(json.dumps(self.records, cls=ResourceEncoder, indent=4))
        except IOError:
            print "I/O error: Could not write to cache"
