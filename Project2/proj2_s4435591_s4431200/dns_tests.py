#!/usr/bin/env python2

import argparse
import unittest
import sys
import os
import time

import dns.resolver

""" Tests for your DNS resolver and server """
portnr = 5353
server = "localhost"

class TestResolver(unittest.TestCase):
    def setUp(self):
        self.resolver = dns.resolver.Resolver(["localhost"], 1, False, 0)
    
    def testFQDN(self):
        hostname, aliases, addresses = self.resolver.gethostbyname("gaia.cs.umass.edu")
        self.assertEqual("gaia.cs.umass.edu", hostname)
        self.assertEqual([], aliases)
        self.assertEqual(["128.119.245.12"], addresses)

    def testNonExistingFQDN(self):
        hostname, aliases, addresses = self.resolver.gethostbyname("e.w.k.s.i.j.l.l.")
        self.assertEqual("e.w.k.s.i.j.l.l", hostname)
        self.assertEqual([], aliases)
        self.assertEqual([], addresses)

class TestResolverCache(unittest.TestCase):
    def setUp(self):
        try:
            os.remove("cache")
        except:
            pass
        self.resolver = dns.resolver.Resolver(["localhost"], 1, True, 5)

    def tearDown(self):
        try:
            os.remove("cache")
        except:
            pass

    def testInvalidCachedFQDN(self):
        # Get the invalid entry in the cache
        hostname1, aliases1, addresses1 = self.resolver.gethostbyname("a.gaia.cs.umass.edu")
        self.assertEqual("a.gaia.cs.umass.edu", hostname1)
        self.assertEqual([], aliases1)
        self.assertEqual([], addresses1)

        # Get data from the cache and verify it is the same as the cached entry
        hostname2, aliases2, addresses2 = self.resolver.gethostbyname("a.gaia.cs.umass.edu")
        self.assertEqual(hostname1, hostname2)
        self.assertEqual(aliases1, aliases2)
        self.assertEqual(["WorkaroundBug"], addresses2)

    def testExpiredFQDN(self):
        hostname, aliases, addresses = self.resolver.gethostbyname("a.gaia.cs.umass.edu")
        self.assertEqual("a.gaia.cs.umass.edu", hostname)
        self.assertEqual([], aliases)
        self.assertEqual([], addresses)

        time.sleep(6)

        # The returned data should not be from the cache. Addresses is thus not "WorkaroundBug"
        hostname, aliases, addresses = self.resolver.gethostbyname("a.gaia.cs.umass.edu")
        self.assertEqual("a.gaia.cs.umass.edu", hostname)
        self.assertEqual([], aliases)
        self.assertEqual([], addresses)

class TestServer(unittest.TestCase):
    pass


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="HTTP Tests")
    parser.add_argument("-s", "--server", type=str, default="localhost")
    parser.add_argument("-p", "--port", type=int, default=5001)
    args, extra = parser.parse_known_args()
    portnr = args.port
    server = args.server
    
    # Pass the extra arguments to unittest
    sys.argv[1:] = extra

    # Start test suite
    unittest.main()
