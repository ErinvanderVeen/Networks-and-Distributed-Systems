#!/usr/bin/env python2.7
import socket
import os
import sys
import struct
import time
import argparse

# all the ICMP types you need
ICMP_ECHO_REQUEST = 8
ICMP_ECHO_REPLY = 0
ICMP_TIME_EXCEEDED = 11

ICMP_MAX_TTL = 10


def checksum(str_):
    str_ = bytearray(str_)
    csum = 0
    countTo = (len(str_) // 2) * 2

    for count in range(0, countTo, 2):
        thisVal = str_[count+1] * 256 + str_[count]
        csum = csum + thisVal
        csum = csum & 0xffffffff

    if countTo < len(str_):
        csum = csum + str_[-1]
        csum = csum & 0xffffffff

    csum = (csum >> 16) + (csum & 0xffff)
    csum = csum + (csum >> 16)
    answer = ~csum
    answer = answer & 0xffff
    answer = answer >> 8 | (answer << 8 & 0xff00)
    return answer


# you should adapt this method to check for the response type
# and based on it, return a (address, responseType) tuple or
# None if no response was received
def receiveOnePing(mySocket, ID, timeout, destAddr):
    startTime = time.time()

    while (startTime + timeout - time.time()) > 0:
        try:
            recPacket, addr = mySocket.recvfrom(1024)
        except socket.timeout:
            break  # timed out
        timeReceived = time.time()

        # Fetch the ICMPHeader fromt the IP
        icmpHeader = recPacket[20:28]
        TTL = ord(struct.unpack("s", recPacket[8:9])[0])

        icmpType, code, checksum, packetID, sequence = struct.unpack(
            "bbHHh", icmpHeader)

        if ((packetID == ID or packetID == 0) and icmpType == ICMP_ECHO_REPLY) or icmpType == ICMP_TIME_EXCEEDED:
            return (addr, icmpType)

    return None

# you should adapt this method so it also sets a TTL value
def sendOnePing(mySocket, destAddr, ID, TTL):

    # Make a dummy header with a 0 checksum
    # Header is type (8), code (8), checksum (16), id (16), sequence (16)
    header = struct.pack("bbHHh",
                         ICMP_ECHO_REQUEST,  # type (byte)
                         0,                  # code (byte)
                         0,                  # checksum (halfword, 2 bytes)
                         ID,                 # ID (halfword)
                         1)                  # sequence (halfword)
    data = struct.pack("d", time.time())
    # Calculate the checksum on the data and the dummy header.
    myChecksum = checksum(header + data)

    # Set the required TTL
    mySocket.setsockopt(socket.SOL_IP, socket.IP_TTL, TTL)

    # Get the right checksum, and put in the header
    if sys.platform == 'darwin':
        # htons: Convert 16-bit integers from host to network byte order
        myChecksum = socket.htons(myChecksum) & 0xffff
    else:
        myChecksum = socket.htons(myChecksum)

    header = struct.pack("bbHHh", ICMP_ECHO_REQUEST, 0, myChecksum, ID, 1)
    packet = header + data

    # AF_INET address must be tuple, not str
    mySocket.sendto(packet, (destAddr, 1))


def doOnePing(destAddr, timeout, TTL):
    icmp = socket.getprotobyname("icmp")
    # SOCK_RAW is a powerful socket type. For more details:
    # http://sock-raw.org/papers/sock_raw

    mySocket = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp)
    mySocket.settimeout(timeout)

    myID = os.getpid() & 0xFFFF  # Return the current process id
    sendOnePing(mySocket, destAddr, myID, TTL)
    result = receiveOnePing(mySocket, myID, timeout, destAddr)

    mySocket.close()
    return result


def traceroute(host, timeout=1.0):
    # timeout=1 means: If one second goes by without a reply from the server,
    # the client assumes that either the client's ping or the server's pong is
    # lost
    TTL = 1
    i = 1
    dest = socket.gethostbyname(host)

    # Send ping requests to a server separated by approximately one second
    while TTL <= ICMP_MAX_TTL:
        answer = doOnePing(dest, timeout, TTL)
        hostname = ""
        if answer == None:
            print(str(i) + ":\t" + "*")
        else:
            (addr, icmpType) = answer
            try:
                hostname = socket.getfqdn(addr[0])
            except:
                pass
            print(str(i) + ":\t" + str(addr[0]) + "\t" + hostname)
            if icmpType == ICMP_ECHO_REPLY:
                break
        TTL += 1
        i += 1

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Trace the route to a given destination.')
    parser.add_argument('destination', type=str, help='an integer for the accumulator')

    args = parser.parse_args()

    traceroute(args.destination)

