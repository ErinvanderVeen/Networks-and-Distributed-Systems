import unittest
import socket
import sys
import time
import gzip
import StringIO

import webhttp.message
import webhttp.parser

portnr = 8001

class TestGetRequests(unittest.TestCase):
    """Test cases for GET requests"""

    def setUp(self):
        """Prepare for testing"""
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(("localhost", portnr))
        self.parser = webhttp.parser.ResponseParser()

    def tearDown(self):
        """Clean up after testing"""
        self.client_socket.shutdown(socket.SHUT_RDWR)
        self.client_socket.close()

    def test_existing_file(self):
        """GET for a single resource that exists"""
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "close")
        self.client_socket.send(str(request))

        # Parse and test response
        message = self.client_socket.recv(1024)
        response = self.parser.parse_response(message)

        self.assertEqual(response.code, 200)
        self.assertTrue(response.body)

    def test_nonexistent_file(self):
        """GET for a single resource that does not exist"""
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/not_index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "close")
        self.client_socket.send(str(request))

        # Parse and test response
        message = self.client_socket.recv(1024)
        response = self.parser.parse_response(message)

        self.assertEqual(response.code, 404)
        self.assertEqual(response.body, '')

    def test_caching(self):
        """GET for an existing single resource followed by a GET for that same
        resource with caching utilized on the client/tester side
        """
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "close")
        self.client_socket.send(str(request))

        # Parse and test response 1
        message = self.client_socket.recv(1024)
        response1 = self.parser.parse_response(message)

        self.assertEqual(response1.code, 200)
        self.assertTrue(response1.body)

        # Recreate socket for second connection since we are not using
        # a persistent connection
        self.tearDown()
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(("localhost", portnr))

        # Set the header for the second request and send it
        request.set_header("If-None-Match", response1.headerdict.get("etag"))
        self.client_socket.send(str(request))

        # Parse and test response 2
        message = self.client_socket.recv(1024)
        response2 = self.parser.parse_response(message)

        self.assertEqual(response2.code, 304)
        self.assertEqual(response2.body, '')

    def test_existing_index_file(self):
        """GET for a directory with an existing index.html file"""
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "close")
        self.client_socket.send(str(request))

        # Parse response for later use
        message = self.client_socket.recv(1024)
        response1 = self.parser.parse_response(message)

        # Recreate socket for second connection since we are not using
        # a persistent connection
        self.tearDown()
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(("localhost", portnr))

        # Change uri to directory for second request
        request.uri = "/test/"
        self.client_socket.send(str(request))

        # Test responses
        message = self.client_socket.recv(1024)
        response2 = self.parser.parse_response(message)

        self.assertEqual(response1.code, 200)
        self.assertEqual(response2.code, 200)
        self.assertEqual(response1.body, response2.body)

    def test_nonexistent_index_file(self):
        """GET for a directory with a non-existent index.html file"""
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/not_index/"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "close")
        self.client_socket.send(str(request))

        # Parse and test response
        message = self.client_socket.recv(1024)
        response = self.parser.parse_response(message)

        self.assertEqual(response.code, 404)
        self.assertEqual(response.body, '')

    def test_persistent_close(self):
        """Multiple GETs over the same (persistent) connection with the last
        GET prompting closing the connection, the connection should be closed.
        """
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "keep-alive")
        self.client_socket.send(str(request))
        # We don't care about this data but we do need to wait to receive it
        self.client_socket.recv(1024)

        # Send the second request
        self.client_socket.send(str(request))
        # We don't care about this data but we do need to wait to receive it
        self.client_socket.recv(1024)

        # Change Connection header to close the connection and send the third request
        request.set_header("Connection", "close")
        self.client_socket.send(str(request))
        # We don't care about this data but we do need to wait to receive it
        self.client_socket.recv(1024)

        # Send the last request so that we can verify that the socket is closed
        self.client_socket.send(str(request))

        # Receive data and check to see if it is indeed empty because the connection was closed
        message = self.client_socket.recv(1024)
        self.assertEqual(message, '')

        # Reopen the socket so that tearDown can close the socket without
        # throwing an error
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(("localhost", portnr))

    def test_persistent_timeout(self):
        """Multiple GETs over the same (persistent) connection, followed by a
        wait during which the connection times out, the connection should be
        closed.
        """
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "keep-alive")
        self.client_socket.send(str(request))
        # We don't care about this data but we do need to wait to receive it
        self.client_socket.recv(1024)

        # Send the second request
        self.client_socket.send(str(request))
        # We don't care about this data but we do need to wait to receive it
        self.client_socket.recv(1024)

        # Cause a timeout to happen. Default is 15 but we want to have ample room so that the test
        # will not accidentally fail if the timing is set too tight
        time.sleep(20)

        # Send the last request so that we can verify that the socket is closed
        self.client_socket.send(str(request))        

        # Receive data and check to see if it is indeed empty because the connection was closed
        message = self.client_socket.recv(1024)
        self.assertEqual(message, '')

        # Reopen the socket so that tearDown can close the socket without
        # throwing an error
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(("localhost", portnr))

    def test_encoding(self):
        """GET which requests an existing resource using gzip encoding, which
        is accepted by the server.
        """
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "keep-alive")
        request.set_header("Accept-Encoding", "gzip;q=0.9")
        self.client_socket.send(str(request))

        # Parse response
        message = self.client_socket.recv(1024)
        response1 = self.parser.parse_response(message)

        # Change to identity encoding so that we can verify that gzip is indeed used
        request.set_header("Accept-Encoding", "identity;q=0.1")
        self.client_socket.send(str(request))

        # Parse second response
        message = self.client_socket.recv(1024)
        response2 = self.parser.parse_response(message)

        # Use gzip on the identity and check whether both requests have the same body
        gbuf = StringIO.StringIO()
        gzip_file = gzip.GzipFile(None, 'wb', 9, gbuf)
        gzip_file.write(response2.body)
        gzip_file.close()

        self.assertEqual(response1.code, 200)
        self.assertEqual(response2.code, 200)
        self.assertEqual(response1.body, gbuf.getvalue())

    def test_no_identity(self):
        """GET for a single resource that exists but no encoding is accepted. (Not even identity)"""
        # Setup the request and send it
        request = webhttp.message.Request()
        request.method = "GET"
        request.uri = "/test/index.html"
        request.set_header("Host", "localhost:{}".format(portnr))
        request.set_header("Connection", "close")
        request.set_header("Accept-Encoding", "identity;q=0")
        self.client_socket.send(str(request))

        # Parse and test response
        message = self.client_socket.recv(1024)
        response = self.parser.parse_response(message)

        self.assertEqual(response.code, 406)
        self.assertEqual(response.body, '')

if __name__ == "__main__":
    # Parse command line arguments
    import argparse
    parser = argparse.ArgumentParser(description="HTTP Tests")
    parser.add_argument("-p", "--port", type=int, default=portnr)
    
    # Arguments for the unittest framework
    parser.add_argument('unittest_args', nargs='*')
    args = parser.parse_args()
    
    # Set port number from argument
    portnr = args.port

    # Only pass the unittest arguments to unittest
    sys.argv[1:] = args.unittest_args

    # Start test suite
    unittest.main()

