"""HTTP Messages

This modules contains classes for representing HTTP responses and requests.
"""

reasondict = {
    # Dictionary for code reasons
    # Format: code : "Reason"
    100 : 'Continue',
    101 : 'Switching Protocols',
    200 : 'OK',
    201 : 'Created',
    202 : 'Accepted',
    203 : 'Non-Authoritative Information',
    204 : 'No Content',
    205 : 'Reset Content',
    206 : 'Partial Content',
    300 : 'Multiple Choices',
    301 : 'Moved Permanently',
    302 : 'Found',
    303 : 'See Other',
    304 : 'Not Modified',
    305 : 'Use Proxy',
    307 : 'Temporary Redirect',
    400 : 'Bad Request',
    401 : 'Unauthorized',
    402 : 'Payment Required',
    403 : 'Forbidden',
    404 : 'Not Found',
    405 : 'Method Not Allowed',
    406 : 'Not Acceptable',
    407 : 'Proxy Authentication Required',
    408 : 'Request Time-out',
    409 : 'Conflict',
    410 : 'Gone',
    411 : 'Length Required',
    412 : 'Precondition Failed',
    413 : 'Request Entity Too Large',
    414 : 'Request-URI Too Large',
    415 : 'Unsupported Media Type',
    416 : 'Requested range not satisfiable',
    417 : 'Expectation Failed',
    500 : 'Internal Server Error',
    501 : 'Not Implemented',
    502 : 'Bad Gateway',
    503 : 'Service Unavailable',
    504 : 'Gateway Time-out',
    505 : 'HTTP Version not supported'
}


class Message(object):
    """Class that stores a HTTP Message"""

    def __init__(self):
        """Initialize the Message"""
        self.version = "HTTP/1.1"
        self.body = ""
        self.headerdict = dict()
        
    def set_header(self, name, value):
        """Add a header and its value
        
        Args:
            name (str): name of header
            value (str): value of header
        """
        # Headers are case insensitive so we convert all headers
        # to lowercase
        self.headerdict[name.lower()] = value

    def get_header(self, name):
        """Get the value of a header
        
        Args:
            name (str): name of header

        Returns:
            str: value of header, empty if header does not exist
        """
        if name.lower() in self.headerdict:
            return self.headerdict[name.lower()]
        else:
            return ""
        
    def __str__(self):
        """Convert the Message to a string
        
        Returns:
            str: representation the can be sent over socket
        """
        message = ""
        for name, value in self.headerdict.iteritems():
            message += name + ": " + str(value) + "\r\n"
        message += "\r\n" + self.body
        return message

    def parse_headers(self, headers):
        """Parse the headers

        Args:
            headers: An array of headers
        """
        for header in headers:
            # Slight tradeoff of speed vs readability
            name = header.split(":")[0]
            value = header.split(":", 1)[1]
            value = value.lstrip()
            self.set_header(name, value)


class Request(Message):
    """Class that stores a HTTP request"""

    def __init__(self):
        """Initialize the Request"""
        super(Request, self).__init__()
        self.method = ""
        self.uri = ""
        
    def __str__(self):
        """Convert the Request to a string

        Returns:
            str: representation the can be sent over socket
        """
        return self.method + " " + self.uri + " " + self.version + "\r\n" + super(Request, self).__str__()

    def parse(self, request):
        """Parse the string request

        Args:
            request: the request
        """
        # The first split drops the body
        # The second splits splits all the headers
        headers = request.split("\r\n\r\n")[0].split("\r\n")
        if len(request.split("\r\n\r\n")) >= 2:
            self.body = request.split("\r\n\r\n")[1]

        # The first line is the startline
        self.method = headers[0].split(" ")[0]
        self.uri = headers[0].split(" ")[1]
        version = headers[0].split(" ")[2]
        headers.pop(0)

        super(Request, self).parse_headers(headers)


class Response(Message):
    """Class that stores a HTTP Response"""

    def __init__(self):
        """Initialize the Response"""
        super(Response, self).__init__()
        self.code = 500
    
    def __str__(self):
        """Convert the Response to a string

        Returns:
            str: representation the can be sent over socket
        """
        return self.version + " " + str(self.code) + " " + reasondict[self.code] + "\r\n" + super(Response, self).__str__()

    def parse(self, request):
        """Parse the string request

        Args:
            request: the request
        """
        # The first split drops the body
        # The second splits splits all the headers
        headers = request.split("\r\n\r\n")[0].split("\r\n")

        self.body = request.split("\r\n\r\n")[1]

        # The first line is the startline
        self.code = int(headers[0].split(" ")[1])
        headers.pop(0)

        super(Response, self).parse_headers(headers)

