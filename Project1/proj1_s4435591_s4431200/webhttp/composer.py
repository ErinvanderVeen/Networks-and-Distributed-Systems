""" Composer for HTTP responses

This module contains a composer, which can compose responses to
HTTP requests from a client.
"""

import time

import webhttp.message
import webhttp.resource
from webhttp.resource import FileExistError
from webhttp.resource import FileAccessError
from webhttp.resource import EncodingNotSupported

class ResponseComposer:
    """Class that composes a HTTP response to a HTTP request"""

    def __init__(self, timeout):
        """Initialize the ResponseComposer
        
        Args:
            timeout (int): connection timeout
        """
        self.timeout = timeout
    
    def compose_response(self, request):
        """Compose a response to a request
        
        Args:
            request (webhttp.Request): request from client

        Returns:
            webhttp.Response: response to request

        """
        response = webhttp.message.Response()

        try:
            resource = webhttp.resource.Resource(request.uri)
            if resource.generate_etag() == request.headerdict.get("if-none-match"):
                response.code = 304
            else:
                try:
                    response.code = 200
                    if request.get_header("Accept-Encoding") == "":
                        response.body = resource.get_content()
                    else:
                        response.body = resource.get_encoded_content(request.get_header("Accept-Encoding"))[0]
                    
                    response.set_header("Content-Length", len(response.body))
                    response.set_header("ETag", resource.generate_etag())
                    response.set_header("Content-Type", resource.get_content_type())
                    response.set_header("Content-Encoding", resource.get_encoded_content(request.get_header("Accept-Encoding"))[1])
                except EncodingNotSupported:
                    response.code = 406
                    response.body = ""
        except FileExistError:
            response.code = 404
        except FileAccessError:
            response.code = 403

        response.set_header("Date", self.make_date_string())

        return response

    def make_date_string(self):
        """Make string of date and time
        
        Returns:
            str: formatted string of date and time
        """
        return time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())

