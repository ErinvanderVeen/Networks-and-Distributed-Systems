"""HTTP Server

This module contains a HTTP server
"""

import threading
import socket
from socket import timeout
import webhttp.parser
import webhttp.composer


class ConnectionHandler(threading.Thread):
    """Connection Handler for HTTP Server"""

    def __init__(self, conn_socket, addr, timeout):
        """Initialize the HTTP Connection Handler
        
        Args:
            conn_socket (socket): socket used for connection with client
            addr (str): ip address of client
            timeout (int): seconds until timeout
        """
        super(ConnectionHandler, self).__init__()
        self.daemon = True
        self.conn_socket = conn_socket
        self.addr = addr
        self.timeout = timeout
        self.keep_alive = True
        
        # 4096 is a typical buffersize
        self.buffer_size = 4096
    
    def handle_connection(self):
        """Handle a new connection"""
        self.conn_socket.settimeout(self.timeout)

        # Create parser and composer
        request_parser = webhttp.parser.RequestParser()
        response_composer = webhttp.composer.ResponseComposer(self.timeout)

        try:
            while self.keep_alive:
                # Receive data send by the client
                recv_data = self.conn_socket.recv(self.buffer_size)

                if not recv_data: break

                # Parse the received data into requests
                requests = request_parser.parse_requests(recv_data)

                for request in requests:
                    # Create a response
                    response = response_composer.compose_response(request)

                    # Send the response
                    self.conn_socket.send(str(response))

                    if request.get_header("Connection") == "close":
                        self.conn_socket.shutdown(socket.SHUT_RDWR)
                        self.keep_alive = False
                        break
        except timeout:
            self.conn_socket.shutdown(socket.SHUT_RDWR)
        except:
            # The client and not the server has closed the socket so there is no for the
            # server to do anything except catch the error so that we can proceed
            # to finally
            pass
        finally:
            # Thread/socket is no longer needed and shutdown has already been called so
            # we only need to free the resources the socket uses
            self.conn_socket.close()
 
    def run(self):
        """Run the thread of the connection handler"""
        self.handle_connection()
        

class Server:
    """HTTP Server"""

    def __init__(self, hostname, server_port, timeout):
        """Initialize the HTTP server
        
        Args:
            hostname (str): hostname of the server
            server_port (int): port that the server is listening on
            timeout (int): seconds until timeout
        """
        self.hostname = hostname
        self.server_port = server_port
        self.timeout = timeout
        self.done = False

    def run(self):
        """Run the HTTP Server and start listening"""

        # Create the socket
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Bind the socket to the hostname and port
        self.socket.bind((self.hostname, self.server_port))

        # Start listening on the socket; python documentation suggests using a value of 5
        self.socket.listen(5)

        while not self.done:
            # Accept the new connection
            conn_socket, client_addr = self.socket.accept()

            # Create a new connection handler for the connection
            handler_thread = ConnectionHandler(conn_socket, client_addr, self.timeout)
            handler_thread.start()
            
    def shutdown(self):
        """Safely shut down the HTTP server"""
        main_thread = threading.current_thread()
        for t in threading.enumerate():
            if t is main_thread:
                continue
            t.keep_alive = False
            t.conn_socket.shutdown(socket.SHUT_RDWR)
            t.join()
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        self.done = True

